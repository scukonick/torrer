import re
import os


def change_tor_geo(torrc, geo, tor_restart_command):
    with open(torrc, 'r') as torrc_file:
        tor_conf = torrc_file.read()
        torrc_file.close()

    tor_conf = re.sub("ExitNodes \{\S+\}", "ExitNodes {{{}}}".format(geo), tor_conf)
    with open(torrc, 'w') as torrc_file:
        torrc_file.write(tor_conf)
        torrc_file.close()

    os.system(tor_restart_command)
