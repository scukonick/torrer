from sim_curl import SimCurl
import yaml
from datetime import datetime
import os
import logging
from tor import change_tor_geo
import time
import random
from pycurl import error as pycurl_error


logger = logging.getLogger(__name__)
ch = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
logger.setLevel(logging.DEBUG)

logger.info('Starting logger')

with open("config.yaml", 'r') as stream:
    config = yaml.load(stream)

proxy_address = config['proxy']
dest_dir = config['dest_dir']
tmp_dir = config['temp_dir']
geos = config['geos']
torrc = '/etc/tor/torrc'
tor_restart_command = config['tor_restart_command']


def check_response(response, filename):
    logger.info("Checking response")
    size = len(response.body_raw)
    if response.response_code != 200:
        logger.error("Response code was wrong: {}".format(response.response_code))
        result = 1
    elif size < 1024:
        logger.error("Body size is less than 1024: {}".format(response.response_code))
        result = 2
    else:
        try:
            old_file = open(os.path.join(dest_dir, filename), 'rb')
            old_file_size = len(old_file.read())
            old_file.close()
        except FileNotFoundError:
            logger.debug("Old file was not found, continue..")
            return 0

        logger.debug("Old file size: {}".format(old_file_size))
        if old_file_size < size * 0.8 or old_file_size > size * 1.2:
            logger.error("New size is pretty different: {} instead of {}".format(old_file_size, size))
            result = 3
        else:
            result = 0

    logger.debug("Check result: {}".format(result))
    return result

while True:

    for geo in geos:
        logger.info("Processing with geo: {}".format(geo))
        change_tor_geo(torrc, geo, tor_restart_command)

        for task in config['tasks']:
            logger.info('Processing url: {}'.format(task['url']))
            now_time = datetime.now().strftime('%Y-%m-%d-%H:%M:%S')
            logger.debug("Now time: {}".format(now_time))
            try:
                r = SimCurl().get(task['url'], sock5_proxy=proxy_address)
            except pycurl_error:
                logger.exception("Problem with downloading url")
                continue
            logger.debug("Response code was: {}".format(r.response_code))

            check_result = check_response(r, task['filename'])

            if check_result == 0:
                logger.info("Response was ok, saving file")
                tmp_file = os.path.join(tmp_dir, task['filename']+"."+now_time)
                logger.debug("Tmp file name: {}".format(tmp_file))

                with open(tmp_file, 'wb') as tmp_file_w:
                    tmp_file_w.write(r.body_raw)
                    tmp_file_w.close()

                dest_file = os.path.join(dest_dir, task['filename'])
                logger.debug("file name: {}".format(dest_file))
                with open(dest_file, 'wb') as dest_file_w:
                    dest_file_w.write(r.body_raw)
                    dest_file_w.close()

                logger.info("Processing to the next url..")
            elif check_result in (2, 3):
                logger.error("Problems with result size, saving to other file")
                dest_file = os.path.join(dest_dir, task['filename'] + '.bad_size')
                logger.debug("file name: {}".format(dest_file))
                with open(dest_file, 'wb') as dest_file_w:
                    dest_file_w.write(r.body_raw)
                    dest_file_w.close()
            elif check_result == 1:
                logger.error("Wrong response code, passing...")
            else:
                logger.error("This should never happen")

        sleep_period = random.randint(config['sleep']['from'], config['sleep']['to'])
        logger.info("Sleeping: {}".format(sleep_period))
        time.sleep(sleep_period)


