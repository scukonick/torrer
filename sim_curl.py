import pycurl
from io import BytesIO, BufferedReader


class SimCurl():
    def get(self, url, sock5_proxy=None):
        curl = pycurl.Curl()

        curl.setopt(pycurl.URL, url)

        b = BytesIO()
        curl.setopt(curl.WRITEFUNCTION, b.write)
        curl.setopt(curl.FOLLOWLOCATION, 1)
        curl.setopt(curl.MAXREDIRS, 5)

        if sock5_proxy is not None:
            curl.setopt(curl.PROXY, sock5_proxy)
            curl.setopt(curl.PROXYTYPE, curl.PROXYTYPE_SOCKS5)
        curl.perform()
        # print(b.getvalue())

        status_code = curl.getinfo(pycurl.HTTP_CODE)

        reader = BufferedReader(b)
        reader.seek(0)
        response_raw = reader.read()

        return Response(status_code, body_raw=response_raw)


class Response():
    def __init__(self, response_code, body=None, body_raw=None):
        self.response_code = response_code
        self.body = body
        self.body_raw = body_raw
